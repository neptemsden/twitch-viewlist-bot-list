# Twitch Viewlist Bot List

This project is a banlist designed to help streamers on [Twitch](https://twitch.tv/) clean from unwanted bots. As a fellow streamer, I understand the importance of maintaining a clean chat environment, although I recognize that opinions on this matter may vary.

The primary goal of this project is to provide a curated list of banned users that can be easily shared and implemented by other streamers. By utilising this banlist, streamers can efficiently maintain a cleaner chat environment and enhance the effectiveness of their moderators when engaging with genuine viewers. This banlist is motivated purely by the desire to combat unwanted bots and maintain a positive streaming experience—there are no religious, political, or judgmental motivations behind this initiative.

We avoid adding bots that are known to be tied to legitimate services such as CommanderRoot, Nightbot, Eklipse, FrostyTools, etc. 

## Getting started

- You'll want to use the [Chat Ban Manager provided by Twitch Tools](https://twitch-tools.rootonline.de/chatban_manager.php) by CommanderRoot. On that site you'll need to sign in and connect your Twitch account. 
- On that site you'll need click the button 'Add new bans' so that it brings up a text input box.
- You'll need to copy and paste all the Bots listed [Botlist.txt](https://gitlab.com/neptemsden/twitch-viewlist-bot-list/-/blob/main/BotList.txt).
- (Optionally) You can list why they're banned (Twitch Viewbot).
- Click 'Add users to banlist'.
- And you're done.

### OR

If you have an Elgato Stream Deck you can download the [Implement Ban List Stream Deck Profile](https://gitlab.com/neptemsden/twitch-viewlist-bot-list/-/tree/main/Stream%20Deck%20Profile?ref_type=heads) that will import a new profile on your stream deck with the current list you downloaded. This can be used by **Channel Owners** AND **Channel Moderators**. 

- Download profile from above link
- Import it via the Elgato Stream Deck tool
- Switch to that profile
- Make sure you've tapped your cursor on the Twitch chat you're adding these bans to
- Press the "BAN ALL" button on the Stream Deck
- Switch back to your profile

Your browser may briefly freeze as it processes the ban requests. Please be patient and avoid clicking anywhere until its done. The system is actively working. Soon, you will see a list of banned channels in the chat and everything will work again. Each time we update Gitlab, a newer version of the profile will be available. Feel free to delete the old version after updating or once you've finished using it!

I have added a button to this Gitlab page, linking to my Twitch channel, as well as a website called Twitch Insights that can help you report new bots to us. Additionally, there are several dummy buttons displaying the profile version and last update. It seems that Elgato packages these profiles into a zip file, so feel free to unzip it and inspect the files inside to ensure there is nothing malicious.

### Note

Regularly updating your banlist is crucial to include new bots and maintain an effective moderation strategy on your Twitch channel. By staying proactive, you can effectively combat emerging viewlist bots and other disruptive elements in your chat. Please note that the determination of illegitimate view bots included in this banlist is based on widely available online tools that anyone can use. The criteria for inclusion are focused on identifying bots that artificially inflate view counts or artificially inflating chatters in the chat list.

The motivation behind creating this banlist stems from frequent requests to share my list of banned users. I recognised the need for a more accessible and centralised resource, which led to the development of this project. Contributions and feedback from the community are encouraged to enhance the effectiveness and accuracy of the banlist. Together, we can create a more secure and enjoyable streaming experience for everyone.

## DISCLAIMER
This banlist is intended solely for the purpose of assisting streamers in managing bot activity on their Twitch channels. The inclusion of any user in this banlist does not imply any personal biases, or opinions related to factors such as religion, politics, defining charatistic, or other personal attributes. The banlist is curated based on disruptive behavior observed within the Twitch community. Streamers are encouraged to use this banlist responsibly and in accordance with Twitch's community guidelines and terms of service. It's important to regularly update the banlist to ensure accuracy and effectiveness in combating unwanted bots and maintaining chat quality. That said this is a not a 100% gaurenteed thing and updates to the list will be made as and when.

## Contributing
Contributions to the banlist are encouraged! If you encounter new bots or have suggestions for improving the banlist, please contribute by opening an issue on GitLab.

Let's work together to create a cleaner and more welcoming environment for all Twitch streamers and viewers.